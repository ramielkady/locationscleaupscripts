SELECT 
    COUNT(t.TrackingNo) totalOrdersEntered,
    t.LocNo
FROM Tracking t
LEFT JOIN Locations l 
ON l.LocNo = t.LocNo
WHERE t.Entered > '11-6-2019'
AND l.LocType = 7215                      -- { This is a "Verified" Location }
AND l.Disabled = 0
GROUP BY t.locno
HAVING COUNT(t.trackingno) > 4            -- { This may be altered to give a greater than or less than for orders by location }
ORDER BY COUNT(t.trackingno) DESC



