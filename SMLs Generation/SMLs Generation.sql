/*************************************************************************
 *
 * SMLs Generation Script V1.0 
 *
 *************************************************************************/


USE BAGROUP;                                                   -- % { Used DB }
IF NOT EXISTS
(
    SELECT *
    FROM sys.objects
    WHERE NAME = 'Master_Loc_tempR'                         -- % { Source table }
          AND TYPE = 'U'
)
    CREATE TABLE BAGROUP.dbo.Master_Loc_tempR                           -- % { Source table }
    (
        Loc_ID INT NOT NULL
    );
-- 

TRUNCATE TABLE BAGROUP.dbo.Master_Loc_tempR  


USE MR8;
-- #region -- $ Insert Master Locations IDs From Excel

INSERT INTO BAGROUP.dbo.Master_Loc_tempR  VALUES (1103)
INSERT INTO BAGROUP.dbo.Master_Loc_tempR  VALUES (1993)

-- #endregion -- $ Insert Master Locations IDs From Excel

-- ! { Select matching "Target" Location records against "Source" records based on "Criteria" }
WITH 
    MatchingTargetLocations AS
    (
        SELECT
            l.LocNo,
            RowCounter,
            l.Disabled,
            SUM
            (
                CASE
                    WHEN trc.Status IN (836, 833, 7844, 6602) 
                    THEN 1
                ELSE 
                    0
                END
            ) Cancelled,
            SUM
            (
                CASE
                    WHEN trc.Status IN (835, 6723, 6538, 6668) 
                    THEN 1
                ELSE
                    0
                END
            ) SuccessParts
        FROM [MR8].[dbo].[Locations] l WITH (NOLOCK)                                         -- % { Target Table }
        RIGHT JOIN [BAGROUP].[dbo].[Copy_of_Ciox_TX GA_with_records_removed(02_23_2021)_Count_391] s    -- % { Source table }
        ON LTRIM(l.Address) LIKE
        (
            CASE
                WHEN LEN([PhysicalAddressStreet1]) - LEN(REPLACE([PhysicalAddressStreet1], ' ', '')) < 2 
                THEN [PhysicalAddressStreet1]
            ELSE 
                RTRIM(LEFT([PhysicalAddressStreet1], CHARINDEX(' ', [PhysicalAddressStreet1], CHARINDEX(' ', [PhysicalAddressStreet1]) + 1)))
            END
        ) + '%'
        AND LocName LIKE SUBSTRING(LTRIM(RTRIM([AccountName])), 1, 10) + '%'
        AND City LIKE s.[PhysicalAddressCity]
        AND PostCode LIKE SUBSTRING(LTRIM(RTRIM(s.[PhysicalAddressZIP])), 1, 5) + '%'
        LEFT JOIN [MR8].[dbo].[Tracking] trc
        ON l.LocNo = trc.LocNo
        GROUP BY 
            l.LocNo,
            RowCounter,
            l.Disabled
    ),


    -- ! { Group matching "Target" Location records based on their originating "Source" record }
    MatchingTargetLocationsGroupedByOriginator AS
    (
        SELECT
            LocNo Loc_ID,
            ROW_NUMBER() OVER (PARTITION BY RowCounter ORDER BY SuccessParts DESC, Cancelled ASC, LocNo ASC) Master_flag,
            Cancelled,
            SuccessParts,
            RowCounter,
            Disabled
        FROM MatchingTargetLocations
    )
--

SELECT *
INTO #tempMatchingTargetLocationsGroupedByOriginator
FROM MatchingTargetLocationsGroupedByOriginator


SELECT
  ms.RowCounter,
  l.*
INTO #tempSuggestedMasterLocations
FROM #tempMatchingTargetLocationsGroupedByOriginator ms
INNER JOIN [MR8].[dbo].[Locations] l
ON ms.Loc_ID = l.LocNo
WHERE Master_flag = 1



-- ! { Selection and display }
-- ? { List Suggested Master Locations - that are not part of the Master List }
SELECT  * 
FROM #tempSuggestedMasterLocations l
LEFT JOIN BAGROUP.dbo.Master_Loc_tempR k
ON k.Loc_ID = l.LocNo 
WHERE l.Disabled = 0 
AND k.Loc_ID IS NULL 


-- ? { List Disabled Suggested Master Locations - that are not part of the Master List }
SELECT  * 
FROM #tempSuggestedMasterLocations l
LEFT JOIN BAGROUP.dbo.Master_Loc_tempR k
ON k.Loc_ID = l.LocNo 
WHERE l.Disabled = 1 
AND k.Loc_ID IS NULL 



-- ? { List Locations records that exist only in the "Source" list }
SELECT s.*
FROM [BAGROUP].[dbo].[Copy_of_Ciox_TX GA_with_records_removed(02_23_2021)_Count_391] s         -- % { Source table }
LEFT JOIN #tempSuggestedMasterLocations m
ON s.RowCounter = m.RowCounter
WHERE m.RowCounter IS NULL

-- ? { List Suggested Master Locations - that are part of the Master List }
SELECT  * 
FROM #tempSuggestedMasterLocations l
INNER JOIN BAGROUP.dbo.Master_Loc_tempR k
ON k.Loc_ID = l.LocNo 


-- ! { Map into "Source" table Matching Target Locations IDs }
UPDATE s 
SET s.Location_ID = l.LocNo 
FROM #tempSuggestedMasterLocations l
INNER JOIN [BAGROUP].[dbo].[Copy_of_Ciox_TX GA_with_records_removed(02_23_2021)_Count_391] s    -- % { Source table }
ON l.RowCounter = s.RowCounter


DROP TABLE #tempSuggestedMasterLocations
DROP TABLE #tempMatchingTargetLocationsGroupedByOriginator






