/*************************************************************************
*
*                            Locations & Parts 
*
*************************************************************************/


/*
Select all Locations with Parts more than 5. 
List Location ID and number of Parts
*/
SELECT l.LocNo, COUNT(t.TrackingNo) 
FROM Locations l
INNER JOIN Tracking t
ON l.LocNo = t.LocNo
GROUP BY l.LocNo
HAVING COUNT(t.TrackingNo) > 5
---------------------------------------------------------------


/*
Select all Locations with Parts more than 4. 
List all Locations details.
*/
SELECT * 
FROM Locations l
WHERE l.LocNo IN 
    (
        SELECT l.LocNo
        FROM Locations l
        INNER JOIN Tracking t
        ON l.LocNo = t.LocNo
        GROUP BY l.LocNo
        HAVING COUNT(t.TrackingNo) > 4
    )
-----------------------------------------------------------------------------------------


/*
Select all Locations with Parts more than 4. 
List all Location details + number of parts. 

*/
WITH Locations_PartsAbv4 AS
    (
        SELECT TOP 10 l.LocNo, COUNT(t.TrackingNo) NumberOfParts
        FROM Locations l
        INNER JOIN Tracking t
        ON l.LocNo = t.LocNo
        GROUP BY l.LocNo
        HAVING COUNT(t.TrackingNo) > 4
    )


SELECT TOP 10 l.*, NumberOfParts 
FROM Locations l
INNER JOIN Locations_PartsAbv4
ON l.LocNo = Locations_PartsAbv4.LocNo     
WHERE l.LocNo IN 
    (
        SELECT TOP 10 l.LocNo
        FROM Locations l
        INNER JOIN Tracking t
        ON l.LocNo = t.LocNo
        GROUP BY l.LocNo
        HAVING COUNT(t.TrackingNo) > 4
    )
-------------------------------------------------------------------------------------------


/*
Select all the Parts that are not associated with any Location  
*/
SELECT * 
FROM Locations l
RIGHT JOIN Tracking t
ON l.LocNo = t.LocNo
WHERE t.LocNo IS NULL
-------------------------------------------------------------------------------------------


/*
Demo the relationship of the below:
Location, Tracking (Part  Retrieval Operation), Case, Code
*/

SELECT  * 
FROM Locations l
WHERE l.LocNo = 77043
    

SELECT * 
FROM Tracking t 
WHERE t.LocNo = 77043


SELECT * 
FROM Cases c
WHERE c.CaseNo = 16356


SELECT *  
FROM Code 
WHERE CodeNo = 2194


-----------------------------------------------------------------------------------------------


/*
Demo relationship between Cases and Tracking 
*/

SELECT * 
FROM Cases c
WHERE c.CaseNo = 16356

SELECT *
FROM Tracking
WHERE CaseNo = 16356