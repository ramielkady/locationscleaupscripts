SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO

CREATE PROC dbo.P_OCPrepayCallListReport
AS
BEGIN
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    CREATE TABLE #Copyboxes
    (
        ContactNo INT NOT NULL,
        Ser INT,
        Copyboxes INT
    );


    INSERT INTO #Copyboxes
    (
        ContactNo,
        Ser,
        Copyboxes
    )
    SELECT wcp.ContactNo,
           1,
           COUNT(WaiverReplyNo) Copyboxes
    FROM MR8.dbo.WaiverReply wr
        JOIN MR8.dbo.CasesParties AS wcp
            ON wcp.CasePartyNo = wr.CasePartyNo
    WHERE 
        Copy = 1
        AND wcp.ContactNo IS NOT NULL
    GROUP BY ContactNo;


    --add an index to enhance performance
    ALTER TABLE #Copyboxes ADD PRIMARY KEY (ContactNo);


    SELECT 

        CASE
            WHEN
                ECP.Entered < IC.PartEnterd 
            THEN
                '"This part is on a case where the OC has declined previous parts.
                 There have been [' + CAST(ECP.Count6896 AS VARCHAR) + '] previous declined parts."'
            --

            WHEN 
                ECP.Entered < IC.DepositoryEntered 
            THEN
                '"This part recently received additional published records and has been included on this report previously"'
            --

            ELSE
                ''
            --    
        END ExceptionNote,


        ISNULL(ECP.Count6896, 0) SecureCopiesNoCopies,
        IC.CasePartyNo,
        IC.IsPatientAtty,
        IC.FirmState,
        IC.OCState,
        IC.FirmCity,
        IC.FirmName,
        IC.FirmNo,
        IC.FirmPhone,
        IC.FirmFax,
        IC.ContactNo,
        IC.ContactType,
        IC.DisplayName,
        IC.OCEmail,
        IC.OCPhone,
        IC.CaseName,
        IC.CaseNo,
        IC.PartNo,
        IC.PartStatus,
        IC.FileType,
        IC.PartEnterd,
        IC.DepositoryEntered,

        --show it only one time per CaseParty
        CASE
            WHEN 
            IC.CasePartyNoser = 1 
            THEN
                SecureCopiesCall.SecureCopiesCallCount
            ELSE
                0
        END SecureCopiesCallCount,

        IC.CaseType,
        IC.TrackingNo,
        ISNULL(Copyboxes.Copyboxes, 0) Copyboxes,
        IC.RecType,
        l.LocName,
		l.Address,
		l.PostCode,
		l.StateNo,
        IC.Pages PagesCount,
        IC.CasePartyNoser
    FROM
    (
        SELECT 

            ROW_NUMBER() OVER (PARTITION BY OC.ContactNo ORDER BY OC.ContactNo) ContactNoser,
            ROW_NUMBER() OVER (PARTITION BY cp.CasePartyNo ORDER BY cp.CasePartyNo) CasePartyNoser,
            cp.CasePartyNo,
            
            CASE
                WHEN 
                cp.IsPatientAtty = 1 
                THEN
                    'Y'
                ELSE
                    'N'
            END IsPatientAtty,

            s.StateName FirmState,
            s.StateNo OCState,
            f.City FirmCity,
            f.FirmName,
            f.FirmNo,
            f.MainPhone FirmPhone,
            f.Fax FirmFax,
            OC.ContactNo,
            ConType.CodeName ContactType,
            OC.DisplayName,
            OC.Email OCEmail,
            OC.MainPhone OCPhone,
            c.CaseName,
            c.CaseNo,
            t.PartNo,
            TStatus.CodeName PartStatus,
            fileType.CodeName FileType,
            t.Entered PartEnterd,
            d.Entered DepositoryEntered,
            caseType.CodeName CaseType,
            c.CaseType CaseTypeNo,
            t.TrackingNo,
            RecType.CodeName RecType,
            t.LocNo,
            i.Pages,
			cp2.BirthDate,
			f.Address,
			d.DpstNo
			   
        FROM MR8.dbo.Cases AS c WITH (NOLOCK)

            JOIN MR8.dbo.Code AS caseType
                ON caseType.CodeNo = c.CaseType         
            --


            --1- All case/parts authorized by subpoena (MR8.dbo.CasesPatients; AuthBy)
            JOIN MR8.dbo.CasesPatients AS cp2 WITH (NOLOCK)
                ON cp2.CaseNo = c.CaseNo
                AND cp2.AuthBy = 'subpoena'             
            --


            JOIN MR8.dbo.CasesParties AS cp WITH (NOLOCK)
                ON cp.CaseNo = c.CaseNo  
            -- 


            JOIN MR8.dbo.Contacts AS OC WITH (NOLOCK)
                ON OC.ContactNo = cp.ContactNo
                AND cp.IsOppSide = 1
                AND OC.ContactType <> 301 --Adjuster  
            --          


            JOIN MR8.dbo.Code AS ConType WITH (NOLOCK)
                ON ConType.CodeNo = OC.ContactType            
            -- 


            --2 - All firms with the following firm types: (MR8.dbo.Firms; FirmType)
            JOIN MR8.dbo.Firms AS f WITH (NOLOCK)
                ON f.FirmNo = OC.FirmNo
                AND f.FirmType IN ( 200, 6908 ) --{Blank} = 200 ,{Opposing Counsel} = 6908
            --
            

            JOIN MR8.dbo.State AS s WITH (NOLOCK)
                ON s.StateNo = f.StateNo
            --
            

            JOIN MR8.dbo.Tracking AS t WITH (NOLOCK)
                ON t.CaseNo = c.CaseNo
                --5 - All case/parts in the following statuses: (MR8.dbo.Tracking; Status)
                /*Records Obtained (835)
                Records Uploaded, Pending Legals (6415)
                ROCKET RUSH Deadline, Pending Legals (6688)
                No Records (834)*/
                AND t.Status IN ( 835, 6415, 6688, 834 )
            --
            

            JOIN MR8.dbo.Code AS TStatus WITH (NOLOCK)
                ON TStatus.CodeNo = t.Status
            --
            

            -- 3 - Published items that have been entered in the past year 
            JOIN MR8.dbo.Items AS i WITH (NOLOCK)
                ON i.TrackingNo = t.TrackingNo
                AND i.Entered > DATEADD(YEAR, -1, GETDATE())
            --
            

            JOIN MR8.dbo.Depository AS d WITH (NOLOCK)
                ON d.ItemNo = i.ItemNo
                AND d.Publish = 1
            --
            

            JOIN MR8.dbo.Code AS fileType
                ON fileType.CodeNo = d.FileType
            --


            JOIN MR8.dbo.Code AS RecType
                ON t.RecType = RecType.CodeNo
            --

    ) IC --IncludedCases


        -- 4 - All Opposing Counsels where the most recent ‘SecureCopies - Call' (6938) Case Party note is older than two weeks.
        JOIN
        (
            SELECT 
                1 CasePartyNoser,
                COUNT(CasePartyLogNo) SecureCopiesCallCount,
                CasePartyNo,
                MAX(Entered) Entered
            FROM MR8.dbo.CasesPartiesLog WITH (NOLOCK)
            WHERE RcvdType = 6938 --SecureCopies - Call
            GROUP BY CasePartyNo
            HAVING MAX(Entered) < DATEADD(DAY, -14, GETDATE())
        ) SecureCopiesCall
            ON SecureCopiesCall.CasePartyNo = IC.CasePartyNo
        --


        JOIN MR8.dbo.Locations AS l
            ON l.LocNo = IC.LocNo
        --


        LEFT OUTER JOIN
        (
            SELECT 
                COUNT(DISTINCT cpl.CasePartyNo) Count6896,
                cpl.CasePartyNo,
                MAX(cpl.Entered) Entered
            FROM MR8.dbo.CasesPartiesLog AS cpl WITH (NOLOCK)
            WHERE cpl.RcvdType = 6896 -- SecureCopies No Copies 
            GROUP BY cpl.CasePartyNo
        ) ECP ---ExcludedCasesParties
        ON ECP.CasePartyNo = IC.CasePartyNo
       
        
        LEFT JOIN #Copyboxes Copyboxes
            ON Copyboxes.ContactNo = IC.ContactNo
            AND Copyboxes.Ser = IC.ContactNoser
            WHERE 
            (
                ECP.CasePartyNo IS NULL
                OR ECP.Entered < IC.PartEnterd
                OR ECP.Entered < IC.DepositoryEntered
            )
        --


        /*Firms that have Scheduling or Ordering checkboxes marked in MR8 (MR8.dbo.CasesParties; IsScheduling = 1 and/or IsOrdering = 1)*/
        AND NOT EXISTS
        (
            SELECT 
                1
            FROM MR8.dbo.Contacts AS CN WITH (NOLOCK)

                JOIN [MR8].dbo.Firms f WITH (NOLOCK)
                ON CN.FirmNo = f.FirmNo

                JOIN [MR8].dbo.CasesParties CPs (NOLOCK)
                ON CN.ContactNo = CPs.ContactNo
                WHERE 
                (
                    CPs.IsOrdering = 1
                    OR CPs.IsScheduling = 1
                )
                    AND IC.FirmNo = f.FirmNo
        )


        /*Opposing counsel firms with a case party note Received Type 7054 - Copies Bill to Client (MR8.dbo.CasePartyLog; RcvdType)*/
        AND NOT EXISTS
        (
            SELECT 1
            FROM MR8.dbo.Contacts AS CN WITH (NOLOCK)

                JOIN [MR8].dbo.Firms f WITH (NOLOCK)
                ON CN.FirmNo = f.FirmNo

                JOIN [MR8].dbo.CasesParties CPs (NOLOCK)
                ON CN.ContactNo = CPs.ContactNo

                JOIN MR8.dbo.CasesPartiesLog cpl WITH (NOLOCK)
                ON cpl.CasePartyNo = CPs.CasePartyNo
                AND cpl.RcvdType = 7054 --Copies Bill to Client 

            WHERE IC.FirmNo = f.FirmNo
        )


        AND NOT EXISTS
        (
            SELECT 1
            FROM MR8.dbo.FirmsTag FT WITH (NOLOCK)
            WHERE (
                    FT.TagName = 'BILL2FIRM'
                    OR FT.TagName = 'NoOCNotifications'
                )
                AND IC.FirmNo = FT.FirmNo
        )


        AND NOT EXISTS
        (
            SELECT 1
            FROM MR8.dbo.WaiverReply WITH (NOLOCK)
            WHERE Copy = 1
                AND WaiverReply.CasePartyNo = IC.CasePartyNo
                AND WaiverReply.TrackingNo = IC.TrackingNo
        )


        AND NOT EXISTS
        (
            SELECT *
            FROM [MR8].dbo.CasesLog CL (NOLOCK) 
            WHERE CL.CaseNo = IC.CaseNo
                AND CL.RcvdType = 7273 -- NoOCNotifications
                AND CL.CancelDate IS NULL
        )
          
        /*Delaware (108)Tennessee (145), Oregon (139), New Mexico (133), Georgia (111), Alabama (101)*/  
        AND IC.OCState NOT IN (108, 145, 139, 133, 111, 101 )

        AND IC.CaseTypeNo <> 707
        --  AND IC.ContactNo = 105455;
    --


    DROP TABLE #Copyboxes;

END;


GO