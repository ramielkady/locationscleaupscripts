/*************************************************************************************************
*
* This script conducts deduping of a Target list base based on a Source list       
* 
**************************************************************************************************/


-- #region -- $ Create Source Temp Table

USE BAGROUP;                                                   -- % { Used DB }

IF object_id('BAGROUP.dbo.Master_Loc_tempR ') IS NOT NULL 
    DROP TABLE BAGROUP.dbo.Master_Loc_tempR  
--

CREATE TABLE BAGROUP.dbo.Master_Loc_tempR                           -- % { Source table }
(
    Loc_ID INT NOT NULL
);
                                 

-- #endregion -- $ END Create Source Temp Table


-- #region --$ Insert Location IDs Into Master Table                          
USE MR8;


-- ! { Insert Loc_IDs values from excel here }
INSERT INTO BAGROUP.dbo.Master_Loc_tempR (Loc_ID) VALUES (183292)     

-- #endregion -- $ END Insert Location IDs Into Master Table


-- #region --$ Remove Dupes From Temp & Add Indexing    

    -- ! { Remove duplicate location numbers } 		
        ;WITH LocIDsDupes AS
        (
            SELECT 
                tl.Loc_ID,
                ROW_NUMBER() OVER (PARTITION BY Loc_ID ORDER BY Loc_ID) ROW_NUM
            FROM BAGROUP.dbo.Master_Loc_tempR AS tl                                  -- % { Source table }
        )
        DELETE FROM LocIDsDupes
        WHERE ROW_NUM > 1;


        -- ! { Add an index to enhance performance }
        ALTER TABLE BAGROUP.dbo.Master_Loc_tempR ADD PRIMARY KEY (Loc_ID)            -- % { Source table }  

-- #endregion --$ END Remove Dupes From Temp & Add Indexing  


-- #region -- $ Setup Handling Mechanism 

        /* -- ! << Handling Mechanism >>
        [Cursor / Processor Block / Source table / Target table / Output table] 
        Temporary table is the Output table  
    --  */


    -- ! { Temp table that will hold duplicative Location reocrds }
    CREATE TABLE #Loc_temp 
    (
        compLocID INT, 
        Loc_ID INT, 
        CompLocName VARCHAR(255), 
        LocName VARCHAR (255), 
        Loc_Address VARCHAR (255), 
        Loc_City VARCHAR (255), 
        Loc_StateNo INT, 
        Loc_PostCode VARCHAR(255), 
        SearchKey VARCHAR(255),     
    )


    -- ! { load in cursor records that will constitute "Comparison records" - becomes Source table }
    DECLARE CUR_Loc CURSOR FOR
        SELECT 
            LocNo, 
            LocName, 
            Address, 
            City, 
            StateNo, 
            PostCode, 
            SearchKey
        FROM   Locations l
        WHERE LocNo IN 
        (
            SELECT Loc_ID                                       
            FROM BAGROUP.dbo.Master_Loc_TempR                           -- % { Source table }
        )
    --
        

    -- ! { PBVs middle variables.They hold match criteria to match against Target table } 
    DECLARE @CursorLocID INT = 1;
    DECLARE @CursorLocNam VARCHAR(255);
    DECLARE @CursorLocAdd VARCHAR(255);
    DECLARE @CursorLocCity VARCHAR(255)
    DECLARE @CursorLocStateNo INT
    DECLARE @CursorLocPostCode VARCHAR (255)
    DECLARE @subAdd NVARCHAR(50)
    DECLARE @CursorSearchKey VARCHAR (255)
    DECLARE @CurosrRowCounter INT                 

-- #endregion -- $ END Setup Handling Mechanism  


-- #region -- $ Truing On Handling Mechanism , Setting up Match Criteria   

    OPEN CUR_Loc
        -- ! { Load first record from cursor - Source - into PBVs }
        FETCH NEXT FROM CUR_Loc 
        INTO 
            @CursorLocID, 
            @CursorLocNam, 
            @CursorLocAdd, 
            @CursorLocCity, 
            @CursorLocStateNo, 
            @CursorLocPostCode, 
            @CursorSearchKey;
        --

        
        WHILE @@FETCH_STATUS = 0
        BEGIN

            -- ! { Setup match criteria for address }
            SELECT  @subAdd= 
                CASE
                    WHEN 
                        LEN(@CursorLocAdd) - LEN(REPLACE(@CursorLocAdd, ' ', '')) < 2 
                    THEN
                        @CursorLocAdd
                    ELSE
                        RTRIM(LEFT(@CursorLocAdd, CHARINDEX(' ', @CursorLocAdd, CHARINDEX(' ', @CursorLocAdd) + 1)))
                END
            --
            print @CursorLocAdd
            print @subAdd

            -- ! { Continue setting  match criteria through PBVs. Load into Output table records from 
            -- !   Target table that match match criteria. Exclude records that match 
            -- !   Comparison records }
            INSERT INTO #loc_temp 
            SELECT  
                @CursorLocID, 
                l.LocNo, 
                @CursorLocNam, 
                l.LocName, 
                l.Address, 
                l.City, 
                l.StateNo, 
                l.PostCode, 
                @CursorSearchKey
            FROM Locations  l                                                   -- % { Target table }
            WHERE LTRIM(Address) LIKE @subAdd + '%' 
            AND LocName LIKE SUBSTRING(LTRIM(RTRIM(@CursorLocNam)),1,10) +'%'
            AND City LIKE @CursorLocCity 
            AND StateNo = @CursorLocStateNo
            AND PostCode LIKE  SUBSTRING(LTRIM(RTRIM(@CursorLocPostCode)),1,5) +'%'
            AND LocType NOT IN (7230)
            AND Disabled = 0
            AND LocNo NOT IN
                (
                    SELECT Loc_ID                                                    
                    FROM BAGROUP.dbo.Master_Loc_TempR                                       -- % { Source table }
                )
            --


            -- ! { Load next reocrd from Source }    
            FETCH NEXT FROM CUR_Loc 
            INTO 
                @CursorLocID, 
                @CursorLocNam,
                @CursorLocAdd, 
                @CursorLocCity, 
                @CursorLocStateNo, 
                @CursorLocPostCode, 
                @CursorSearchKey;

        END
    CLOSE CUR_Loc
    DEALLOCATE CUR_Loc

-- #endregion -- $ END Truing On Handling Mechanism , Setting up Match Criteria 


-- #region -- $ Selection & Display 

    SELECT
        #Loc_temp.CompLocName CompLocName
        ,#Loc_temp.compLocID CompLocID 
        ,#Loc_temp.SearchKey CompSearchKey
        ,l.LocNo DupeLocNo
        ,l.LocName DupLocName
        ,l.Address DupeAddress
        ,l.City DupeCity
        ,st.StateName DupeState
        ,l.PostCode DupeZip
        ,l.MainPhone DupeMainPhone
        ,l.Contact DupeContact
        ,l.Warning DupeWarning
        ,loctype.CodeName DupeLocationType
        ,l.Disabled DupeInactive
        ,l.SearchKey DupeSearchKey
        ,dept.CodeName DupeDepartment
    FROM locations l                                                   -- % { Target table }
    JOIN #Loc_temp ON #Loc_temp.Loc_ID = l.LocNo
    JOIN Code dept ON dept.CodeNo = l.Department
    JOIN State st ON st.StateNo = l.StateNo
    JOIN code loctype ON loctype.CodeNo = l.LocType    

-- #endregion -- $ END Selection & Display


-- #region -- $ Drop Temp Tables 

    DROP TABLE #Loc_temp    

-- #endregion -- $ Drop Temp Tables  






