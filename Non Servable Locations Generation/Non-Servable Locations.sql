/****************************************************************************************************
*
* The purpose of this script is to produce Duplicative Non - Servable Location records from a Target table based on 
* Source and Filter tables 
* 
*****************************************************************************************************/

-- #region -- $ Comments
/*****************************************************************************************************

-- ! <<<INPUT>>>
1. IDed list of inactive Master Locations (Source list). 
2. Match Criteria -->  to lock on duplicative records of Target. 
3. (Target list) -->  Where duplicative of inactive Masters exist.
4, (Filter list) -->  to limit – filter – range in Target list.   


-- ! <<<OUTPUT>>>
IDed duplicative records of Master Locations   


-- !  <<<TEST DATA>>>
-- SELECT * 
-- FROM   Locations
-- WHERE LocNo in(1102712, 695964, 183019, 712986)

*******************************************************************************************************/
-- #endregion -- $ END Comments


-- #region -- $ Setup Source, Target, and Output tables. [T] PI-SFTO mechanism setup
/*
[Processor block / Table iterator / Source table / Filter table / Target table / Output table] 
*/

-- ! { Output temp table to hold Suggested Duplicative Locations }
CREATE TABLE #Loc_temp 
    (
        Loc_ID INT, 
        LocName VARCHAR (255), 
        Loc_Address VARCHAR (255)
    )
--


-- ! { Source temp table that will hold IDs of inactive Masters}
CREATE TABLE #Source_table 
    (
        LocNo INT, 
        LocName VARCHAR (255), 
        Address VARCHAR (255),
        Iterator INT
    )
--


-- ! { Load inactive Masters into Source temp table } 
    INSERT INTO #Source_table 
    (
        LocNo, 
        LocName, 
        Address
    )    
    SELECT LocNo, LocName, Address    
    FROM   Locations                                       -- % { Source table }
	WHERE LocNo IN (1075284)
    /*
    This needs to be a list of all inactivated invalid serve locations. 
    ie: a publix pharmacy is invalid in contrast to the publix corporate HQ - which is valid
    */
--	


-- ! { Update the P-IO table iterator iterator column }
UPDATE #Source_table
SET Iterator = 1;


-- ! { PBVs to establish match criteria between Source temp and Target tables }
DECLARE @CursorLocID INT = 1;
DECLARE @CursorLocNam VARCHAR(255);
DECLARE @CursorLocAdd VARCHAR(255);

-- #endregion -- $ Setup Source, Target, and Output tables. [T] PC-SFTO mechanism setup
 

-- #region -- $ PC-SFTO mechanism turning on, setup match criteria
/*
A Table Iterator component utilizes a P-IO mechanism.
[Processor block / Input Table / Output table] mechanism 
*/

-- ! { Loop over temp Source, Setup match criteria and load into Target }
WHILE EXISTS 
    (
        SELECT 1 FROM #Source_table WHERE Iterator = 1
    )

BEGIN

    SELECT TOP (1)
		@CursorLocID = LocNo,
        @CursorLocNam = LocName,
        @CursorLocAdd = Address     
    FROM #Source_table  
    WHERE Iterator = 1
    --
    
    -- % { Alternative match criteria }
    -- INSERT INTO #Loc_temp 
    -- SELECT locNo, LocName, Address 
    -- FROM Locations 
    -- WHERE Address LIKE '%'+ SUBSTRING(LTRIM(RTRIM(@CursorLocAdd)),1,10) +'%' 
    -- AND LocName LIKE SUBSTRING(LTRIM(RTRIM(@CursorLocNam)),1,10) +'%'


    -- % { Alternative match criteria }
    -- INSERT INTO #Loc_temp 
    -- SELECT locNo, LocName, Address 
    -- FROM Locations WHERE Address LIKE '%' 
    -- AND LocName LIKE SUBSTRING(LTRIM(RTRIM(@CursorLocNam)),1,10) +'%'
        

    -- ! { Load into Output table records that meet match criteria }
    /*
    Load into Output table records from Source temp table that meet match criteria set in PBVs - 
    excluding records in Filter table (include filter criteria) 
    */

    INSERT INTO #Loc_temp 
    SELECT Locations.locNo, LocName, Address 
    FROM Locations 
    WHERE Address LIKE '%' 
    AND LocName LIKE LTRIM(RTRIM(@CursorLocNam)) +'%'        -- % { Match Criteria set here }
    AND LocType NOT IN (7230)                                -- & { What is this ? }
    AND Disabled = 0
    AND  Locations.LocNo NOT IN 
        (
            SELECT ml.LocNo 
            FROM Locations loc
            INNER JOIN KRS.dbo.MasterList ml     -- % { Filter table }
            ON loc.LocNo = ml.LocNo
            WHERE ml.MasterLocType = 2
        )
    --            

    UPDATE #Source_table 
    SET Iterator = NULL 
    WHERE LocNo = @CursorLocID
END  

-- #endregion -- $ END PC-SFTO mechanism turning on, setup match criteria 


-- #region -- $ Selection & diplay

SELECT DISTINCT * 
FROM locations 
INNER JOIN #Loc_temp 
ON #Loc_temp.Loc_ID = Locations.LocNo


DROP TABLE #Loc_temp
DROP TABLE #Source_table

-- #endregion -- $ END Selection & diplay



